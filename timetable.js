var RichDate = (function () {
    function RichDate(date) {
        this.date = new Date();
        this.date.setSeconds(0);
        if (date !== undefined) {
            if (date instanceof Date)
                this.date = date;
            else if (typeof date === "string") {
                var hours = parseInt(date.substr(0, 2));
                var minutes = parseInt(date.substr(2, 2));
                this.date.setHours(hours);
                this.date.setMinutes(minutes);
            }
            else {
                this.setDay(Math.floor(date / (24 * 60)));
                var minutesThisDay = date % (24 * 60);
                this.date.setHours(minutesThisDay / 60);
                this.date.setMinutes(minutesThisDay % 60);
            }
        }
    }
    RichDate.prototype.getDay = function () {
        return (this.date.getDay() === 0) ? 6 : this.date.getDay() - 1;
    };
    RichDate.prototype.getHours = function () {
        return this.date.getHours();
    };
    RichDate.prototype.getMinutes = function () {
        return this.date.getMinutes();
    };
    RichDate.prototype.toString = function (withSeparator) {
        if (withSeparator === void 0) { withSeparator = true; }
        function to2Digits(input) {
            var res = (input < 10) ? "0" : "";
            res += input.toString();
            return res;
        }
        var separator = (withSeparator) ? ':' : '';
        return to2Digits(this.date.getHours()) + separator + to2Digits(this.date.getMinutes());
    };
    RichDate.prototype.setTime = function (time) {
        this.date.setHours(time.substr(0, 2));
        this.date.setMinutes(time.substr(2, 2));
    };
    RichDate.prototype.setDay = function (daytoset) {
        var now = new RichDate();
        var distance = daytoset - now.getDay();
        distance = (distance < 0) ? distance + 7 : distance;
        this.date.setDate(now.date.getDate() + distance);
    };
    RichDate.prototype.dayBefore = function () {
        return (this.getDay() === 0) ? 6 : this.getDay() - 1;
    };
    RichDate.prototype.dayAfter = function () {
        return (this.getDay() === 6) ? 0 : this.getDay() + 1;
    };
    RichDate.prototype.inMinutes = function () {
        return this.date.getHours() * 60 + this.date.getMinutes();
    };
    RichDate.prototype.getMinutesFromMonday = function () {
        return (this.getDay() * (24 * 60) + this.inMinutes());
    };
    return RichDate;
}());
var HourInterval = (function () {
    function HourInterval(from, to) {
        if (from instanceof RichDate) {
            this.from = from;
            this.to = to;
        }
        else {
            this.from = new RichDate(from);
            this.to = new RichDate(to);
        }
    }
    HourInterval.prototype.isEqual = function (intervalB) {
        if (this.from.inMinutes() === intervalB.from.inMinutes()
            && this.to.inMinutes() === intervalB.to.inMinutes()) {
            return true;
        }
        return false;
    };
    HourInterval.prototype.toString = function () {
        return this.from.toString() + ' - ' + this.to.toString();
    };
    return HourInterval;
}());
var WeeklyInterval = (function () {
    function WeeklyInterval(arg) {
        this.days = [false, false, false, false, false, false, false];
        this.hours = new HourInterval(new RichDate(), new RichDate());
        if (arg !== undefined) {
            if (typeof arg === 'string') {
                this.days = [false, false, false, false, false, false, false];
                for (var i = 0; arg[i] !== 'T'; i++)
                    this.days[Number(arg[i]) - 1] = true;
                var slashIndex = arg.indexOf('/');
                this.hours.from = this.parseHour(arg.substr(slashIndex - 5, 5));
                this.hours.to = this.parseHour(arg.substr(slashIndex + 1, 5));
            }
            else if (typeof arg === 'object') {
                this.days = arg.days;
                this.hours = arg.hours;
            }
        }
    }
    WeeklyInterval.prototype.getDays = function () {
        return this.days;
    };
    WeeklyInterval.prototype.setDays = function (days) {
        this.days = days;
    };
    WeeklyInterval.prototype.addDay = function (day) {
        this.days[day] = true;
    };
    WeeklyInterval.prototype.removeDay = function (day) {
        this.days[day] = false;
    };
    WeeklyInterval.prototype.stepsOverNextDay = function () {
        return (this.hours.to.inMinutes() < this.hours.from.inMinutes());
    };
    ;
    WeeklyInterval.prototype.containsDay = function (day) {
        if (!day)
            day = new RichDate().getDay();
        return (this.days[day]);
    };
    WeeklyInterval.prototype.getHours = function () {
        return this.hours;
    };
    WeeklyInterval.prototype.setHours = function (hours) {
        if (hours instanceof HourInterval)
            this.hours = hours;
        else
            this.hours = new HourInterval(hours.from, hours.to);
    };
    WeeklyInterval.prototype.getDuration = function () {
        if (!this.stepsOverNextDay())
            return this.hours.to.inMinutes() - this.hours.from.inMinutes();
        else
            return (24 * 60) - this.hours.from.inMinutes() + this.hours.to.inMinutes();
    };
    WeeklyInterval.prototype.toString = function () {
        var res = "";
        for (var i = 0; i < this.days.length; i++) {
            if (this.days[i])
                res += (i + 1);
        }
        res += 'T';
        res += this.hours.from.toString();
        res += '/';
        res += this.hours.to.toString();
        return res;
    };
    WeeklyInterval.prototype.isEqual = function (intervalB) {
        return (intervalB.toString() === this.toString());
    };
    WeeklyInterval.prototype.parseHour = function (hour) {
        var res = new RichDate();
        res.setTime(hour.substr(0, 2) + hour.substr(3, 2));
        return res;
    };
    return WeeklyInterval;
}());
var Timeline = (function () {
    function Timeline(arg) {
        this.segments = [];
        this.intervals = [];
        if (typeof arg === "string" && arg !== "") {
            var splitArg = arg.split('|');
            for (var i = 0; i < splitArg.length; i++) {
                var splitSegment = splitArg[i].split('-');
                this.segments.push([
                    Number(splitSegment[0]),
                    Number(splitSegment[1])
                ]);
            }
            this.intervals = this.toWeeklyIntervals();
        }
        else if (typeof arg === "object") {
            this.addInterval(arg);
        }
    }
    Timeline.prototype.indexOfSegment = function (segment) {
        for (var i = 0; i < this.segments.length; i++) {
            if (this.segments[i][0] === segment[0] && this.segments[i][1] === segment[1])
                return i;
        }
        return -1;
    };
    Timeline.prototype.addInterval = function (interval) {
        var intervals = [].concat(interval);
        for (var i = 0; i < intervals.length; i++) {
            var interval = intervals[i];
            for (var d = 0; d < 7; d++) {
                if (interval.days[d]) {
                    var from = (d * 24 * 60) + (interval.hours.from.inMinutes());
                    var to = from + interval.getDuration();
                    to = (to > (24 * 60 * 7)) ? to - (24 * 60 * 7) : to;
                    var mergedSegment = this.mergeSegmentsIn(from, to);
                    this.segments.splice(this.findPosition(mergedSegment[0]), 0, mergedSegment);
                }
            }
        }
        this.intervals = this.toWeeklyIntervals();
    };
    Timeline.prototype.removeInterval = function (index) {
        this.intervals.splice(index, 1);
        this.segments = [];
        this.addInterval(this.intervals);
    };
    Timeline.prototype.editInterval = function (index, edited) {
        this.removeInterval(index);
        this.addInterval(edited);
    };
    Timeline.prototype.removeSegment = function (segment) {
        var segmentIndex = typeof segment === "object"
            ? this.indexOfSegment(segment)
            : segment;
        if (segmentIndex === -1)
            return false;
        this.segments.splice(segmentIndex, 1);
        return true;
    };
    Timeline.prototype.toWeekDays = function () {
        var weekdays = new Array();
        var segments = this.truncate(3 * 60);
        for (var i = 0; i < segments.length; i++) {
            var day = Math.floor(segments[i][0] / (24 * 60));
            if (!weekdays[day])
                weekdays[day] = new Array();
            weekdays[day].push(this.segmentToStr(segments[i]));
        }
        return weekdays;
    };
    Timeline.prototype.toWeeklyIntervals = function () {
        var res = [];
        var found = false;
        for (var a = 0; a < this.segments.length; a++) {
            var from = this.segments[a][0] % (24 * 60);
            var to = this.segments[a][1] % (24 * 60);
            var compareWith = new HourInterval(from, to);
            var day = Math.floor(this.segments[a][0] / (24 * 60));
            for (var z = 0; z < res.length; z++) {
                if (res[z].hours.isEqual(compareWith)) {
                    res[z].addDay(day);
                    found = true;
                }
            }
            if (!found) {
                var newInterval = new WeeklyInterval();
                newInterval.addDay(day);
                newInterval.setHours(new HourInterval(from, to));
                res.push(newInterval);
            }
            found = false;
        }
        return res;
    };
    Timeline.prototype.toString = function () {
        var res = "";
        for (var i = 0; i < this.segments.length; i++) {
            res += this.segments[i][0] + '-' + this.segments[i][1];
            if (i + 1 < this.segments.length)
                res += '|';
        }
        return res;
    };
    Timeline.prototype.isHappening = function () {
        var now = new RichDate().getMinutesFromMonday();
        for (var i = 0; i < this.segments.length; i++) {
            if (now >= this.segments[i][0] && now <= this.segments[i][1])
                return true;
        }
        return false;
    };
    Timeline.prototype.isHappeningOn = function () {
        var now = new RichDate().getMinutesFromMonday();
        for (var i = 0; i < this.segments.length; i++) {
            if (now >= this.segments[i][0] && now <= this.segments[i][1])
                return this.segments[i];
        }
        return false;
    };
    Timeline.prototype.changesIn = function () {
        if (this.segments.length === 0)
            return -1;
        var now = new RichDate().getMinutesFromMonday();
        var segmentFound = this.isHappeningOn();
        if (!segmentFound) {
            var nextSegment = this.segments[this.findNextSegment(now)];
            return (nextSegment[0] - now);
        }
        else
            return segmentFound[1] - now;
    };
    Timeline.prototype.changesAt = function () {
        if (this.segments.length === 0)
            return -1;
        var now = new RichDate().getMinutesFromMonday();
        var segmentFound = this.isHappeningOn();
        if (!segmentFound) {
            var nextSegment = this.segments[this.findNextSegment(now)];
            return (new RichDate(nextSegment[0]));
        }
        else
            return new RichDate(segmentFound[1]);
    };
    Timeline.prototype.mergeSegmentsIn = function (from, to) {
        var res = [from, to];
        function flatTo(fromArg, toArg) {
            return (toArg < fromArg) ? (24 * 60 * 7) + toArg : toArg;
        }
        for (var i = 0; i < this.segments.length; i++) {
            var isExcluded = ((this.segments[i][0] < from && flatTo(this.segments[i][0], this.segments[i][1]) < from)
                || (from < this.segments[i][0] && flatTo(from, to) < this.segments[i][0]));
            if (!isExcluded) {
                if (this.segments[i][0] < res[0])
                    res[0] = this.segments[i][0];
                if (flatTo(this.segments[i][0], this.segments[i][1]) > flatTo(res[0], res[1]))
                    res[1] = this.segments[i][1];
                this.segments.splice(i, 1);
            }
        }
        return res;
    };
    Timeline.prototype.minutesToStr = function (minutes) {
        var res = "";
        res = (Math.floor(minutes / 60)).toString();
        res += ":";
        res += (minutes % 60).toString();
        res += (minutes % 60 < 10) ? "0" : "";
        return res;
    };
    Timeline.prototype.segmentToStr = function (segment) {
        var hourFrom = segment[0] % (24 * 60);
        var hourTo = segment[1] % (24 * 60);
        return this.minutesToStr(hourFrom) + " - " + this.minutesToStr(hourTo);
    };
    Timeline.prototype.truncate = function (dayStartsAt) {
        if (dayStartsAt === void 0) { dayStartsAt = 0; }
        var res = [];
        var lengthDay = (24 * 60);
        for (var i = 0; i < this.segments.length; i++) {
            var day1 = Math.floor((this.segments[i][0] - dayStartsAt) / lengthDay);
            var day2 = Math.floor((this.segments[i][1] - dayStartsAt) / lengthDay);
            if (day1 !== day2) {
                var cutAt = (day1 === 6)
                    ? dayStartsAt
                    : day2 * lengthDay + dayStartsAt;
                res.push([this.segments[i][0], cutAt]);
                res.push([cutAt, this.segments[i][1]]);
            }
            else {
                res.push(this.segments[i]);
            }
        }
        return res;
    };
    Timeline.prototype.findPosition = function (from) {
        for (var i = 0; i < this.segments.length; i++) {
            if (from < this.segments[i][0])
                return i;
        }
        return this.segments.length;
    };
    Timeline.prototype.findNextSegment = function (from) {
        return this.findPosition(from) === this.segments.length
            ? 0
            : this.findPosition(from);
    };
    return Timeline;
}());
