/*
########  ####  ######  ##     ## ########     ###    ######## ########
##     ##  ##  ##    ## ##     ## ##     ##   ## ##      ##    ##
##     ##  ##  ##       ##     ## ##     ##  ##   ##     ##    ##
########   ##  ##       ######### ##     ## ##     ##    ##    ######
##   ##    ##  ##       ##     ## ##     ## #########    ##    ##
##    ##   ##  ##    ## ##     ## ##     ## ##     ##    ##    ##
##     ## ####  ######  ##     ## ########  ##     ##    ##    ########
*/

// RichDate is used to empower Date
class RichDate {
  date: Date;

  constructor();
  constructor(date: Date);
  constructor(date: string);
  constructor(date: number); // input minutes from 00:00
  constructor(date?: any) {

    this.date = new Date();
    this.date.setSeconds(0);

    if (date !== undefined) {
      if (date instanceof Date)
        this.date = date;
      else if (typeof date === "string") {
        var hours = parseInt(date.substr(0, 2));
        var minutes = parseInt(date.substr(2, 2));
        this.date.setHours(hours);
        this.date.setMinutes(minutes);
      }
      else {
        this.setDay(Math.floor(date / (24 * 60)));
        var minutesThisDay = date % (24 * 60)
        this.date.setHours(minutesThisDay / 60);
        this.date.setMinutes(minutesThisDay % 60);
      }
    }
  }

  // except for the input method, the days are indexed as monday = 0
  getDay() {
    return (this.date.getDay() === 0) ? 6 : this.date.getDay() - 1;
  }

  getHours() {
    return this.date.getHours();
  }

  getMinutes() {
    return this.date.getMinutes();
  }

  toString(withSeparator: boolean = true) {

    function to2Digits(input: number): string {
      var res = (input < 10) ? "0": "";
      res += input.toString();
      return res;
    }

    var separator = (withSeparator) ? ':' : '';

    return to2Digits(this.date.getHours()) + separator + to2Digits(this.date.getMinutes());
  }

  // input time as like hhmm
  setTime(time) {
    this.date.setHours(time.substr(0, 2));
    this.date.setMinutes(time.substr(2, 2));
  }

  setDay(daytoset) {
    var now = new RichDate();
    var distance = daytoset - now.getDay();

    // we always set to the next occurence
    distance = (distance < 0) ? distance + 7 : distance;

    this.date.setDate(now.date.getDate() + distance);
  }

  // the day of the week before the current one
  dayBefore() {
    return (this.getDay() === 0) ? 6 : this.getDay() - 1;
  }

  dayAfter() {
    return (this.getDay() === 6) ? 0 : this.getDay() + 1;
  }

  // # minutes from 00:00 at this day
  inMinutes() {
    return this.date.getHours() * 60 + this.date.getMinutes();
  }

  // # minutes from monday at 00:00 of this week
  getMinutesFromMonday() {
    return (this.getDay() * (24 * 60) + this.inMinutes());
  }
}


/*
#### ##    ## ######## ######## ########  ##     ##    ###    ##
 ##  ###   ##    ##    ##       ##     ## ##     ##   ## ##   ##
 ##  ####  ##    ##    ##       ##     ## ##     ##  ##   ##  ##
 ##  ## ## ##    ##    ######   ########  ##     ## ##     ## ##
 ##  ##  ####    ##    ##       ##   ##    ##   ##  ######### ##
 ##  ##   ###    ##    ##       ##    ##    ## ##   ##     ## ##
#### ##    ##    ##    ######## ##     ##    ###    ##     ## ########
*/

// A simple container for 2 RichDates (from and to)
class HourInterval {
  public from: RichDate;
  public to: RichDate;

  public constructor(from: RichDate, to: RichDate);
  public constructor(from: string, to: string);
  public constructor(from: number, to: number); // minutes from 00:00
  public constructor(from: any, to: any) {
    if (from instanceof RichDate) {
      this.from = from;
      this.to = to;
    }
    else {
      this.from = new RichDate(from);
      this.to = new RichDate(to);
    }
  }

  public isEqual(intervalB: HourInterval) {
    if (this.from.inMinutes() === intervalB.from.inMinutes()
    && this.to.inMinutes() === intervalB.to.inMinutes()) {
      return true;
    }
    return false;
  }

  public toString() {
    return this.from.toString() + ' - ' + this.to.toString();
  }
}


/*
##      ## ######## ######## ##    ## ##       ##    ##
##  ##  ## ##       ##       ##   ##  ##        ##  ##
##  ##  ## ##       ##       ##  ##   ##         ####
##  ##  ## ######   ######   #####    ##          ##
##  ##  ## ##       ##       ##  ##   ##          ##
##  ##  ## ##       ##       ##   ##  ##          ##
 ###  ###  ######## ######## ##    ## ########    ##
*/

// Combination of an hour interval with a selection of days
// is used to facilitate input by editing multiple days at once
class WeeklyInterval {

  public days: boolean[];
  public hours: HourInterval;

  /*
   #####  #    # #####  #      #  ####
   #    # #    # #    # #      # #    #
   #    # #    # #####  #      # #
   #####  #    # #    # #      # #
   #      #    # #    # #      # #    #
   #       ####  #####  ###### #  ####
  */

  public constructor();
  public constructor(arg: Object);
  public constructor(arg: string);
  public constructor(arg?: any) {
    this.days = [false, false, false, false, false, false, false];
    this.hours = new HourInterval(new RichDate(), new RichDate());

    if (arg !== undefined) {

      // Constructs from string
      if (typeof arg === 'string') {
        this.days = [false, false, false, false, false, false, false];
        for (var i = 0; arg[i] !== 'T'; i ++)
          this.days[Number(arg[i]) - 1] = true;

        var slashIndex = arg.indexOf('/');
        this.hours.from = this.parseHour(arg.substr(slashIndex - 5, 5));
        this.hours.to = this.parseHour(arg.substr(slashIndex + 1, 5));
      }
      // or constructs from 'days' and 'hours': HourInterval arguments
      else if (typeof arg === 'object') {
        this.days = arg.days;
        this.hours = arg.hours;
      }
    }
  }

  public getDays() {
    return this.days;
  }

  public setDays(days) {
    this.days = days;
  }

  public addDay(day) {
    this.days[day] = true;
  }

  public removeDay(day) {
    this.days[day] = false;
  }

  // returns true if the interval is on 2 days
  public stepsOverNextDay() {
    return (this.hours.to.inMinutes() < this.hours.from.inMinutes());
  };

  public containsDay(day?: number) {
    if (!day)
    day = new RichDate().getDay();
    return (this.days[day]);
  }

  public getHours() {
    return this.hours;
  }

  // input as HourInterval or {from: "hhmm", to: "hhmm"}
  public setHours(hours: HourInterval);
  public setHours(hours: any) {
    if (hours instanceof HourInterval)
      this.hours = hours;
    else
      this.hours = new HourInterval(hours.from, hours.to);
  }

  public getDuration() {
    if (!this.stepsOverNextDay())
      return this.hours.to.inMinutes() - this.hours.from.inMinutes();
    else
      return (24 * 60) - this.hours.from.inMinutes() + this.hours.to.inMinutes();
  }

  public toString() {
    var res = "";
    for (var i = 0; i < this.days.length; i++) {
      if (this.days[i])
        res += (i + 1);
    }
    res += 'T';
    res += this.hours.from.toString();
    res += '/';
    res += this.hours.to.toString();
    return res;
  }

  public isEqual(intervalB: WeeklyInterval): boolean {
    return (intervalB.toString() === this.toString());
  }

  /*
   #####  #####  # #    #   ##   ##### ######
   #    # #    # # #    #  #  #    #   #
   #    # #    # # #    # #    #   #   #####
   #####  #####  # #    # ######   #   #
   #      #   #  #  #  #  #    #   #   #
   #      #    # #   ##   #    #   #   ######
  */

  private parseHour(hour: string) {
    var res = new RichDate();
    res.setTime(hour.substr(0, 2) + hour.substr(3, 2));
    return res;
  }
}


/*
######## #### ##     ## ######## ##       #### ##    ## ########
   ##     ##  ###   ### ##       ##        ##  ###   ## ##
   ##     ##  #### #### ##       ##        ##  ####  ## ##
   ##     ##  ## ### ## ######   ##        ##  ## ## ## ######
   ##     ##  ##     ## ##       ##        ##  ##  #### ##
   ##     ##  ##     ## ##       ##        ##  ##   ### ##
   ##    #### ##     ## ######## ######## #### ##    ## ########
*/

// Representation of a weekly timetable as a line graduated in minutes (starting on monday)
// Is punctuated by segments that represent the state of true (ex: when a shop is open)
class Timeline {

  public segments: number[][];
  private intervals: WeeklyInterval[];

  /*
  #####  #    # #####  #      #  ####
  #    # #    # #    # #      # #    #
  #    # #    # #####  #      # #
  #####  #    # #    # #      # #
  #      #    # #    # #      # #    #
  #       ####  #####  ###### #  ####
  */

  // you can pass an array of WeeklyInterval
  public constructor(arg: string);
  public constructor(arg: WeeklyInterval[]);
  public constructor(arg?: any) {
    this.segments = [];
    this.intervals = [];

    if (typeof arg === "string" && arg !== "") {
      var splitArg = arg.split('|');
      for (var i = 0; i < splitArg.length; i++) {
        var splitSegment = splitArg[i].split('-');
        this.segments.push([
          Number(splitSegment[0]),
          Number(splitSegment[1])
        ]);
      }
      this.intervals = this.toWeeklyIntervals();
    }
    else if (typeof arg === "object") {
      this.addInterval(arg);
    }
  }

  // returns the index of the matching segment (or -1)
  public indexOfSegment(segment: number[]): number {
    for (var i = 0; i < this.segments.length; i++) {
      if (this.segments[i][0] === segment[0] && this.segments[i][1] === segment[1])
        return i;
    }
    return -1;
  }

  // adds a segment to the timeline from a WeeklyInterval
  public addInterval(interval: WeeklyInterval);
  public addInterval(interval: WeeklyInterval[]);
  public addInterval(interval: any) {

    var intervals = [].concat(interval);

    for (var i = 0; i < intervals.length; i++) {
      var interval = intervals[i];

      for (var d = 0; d < 7; d++) {
        if (interval.days[d]) {

          var from = (d * 24 * 60) + (interval.hours.from.inMinutes());
          var to = from + interval.getDuration();

          to = (to > (24 * 60 * 7)) ? to - (24 * 60 * 7) : to;

          var mergedSegment = this.mergeSegmentsIn(from, to);

          this.segments.splice(this.findPosition(mergedSegment[0]), 0, mergedSegment);
        }
      }
    }
    this.intervals = this.toWeeklyIntervals();
  }

  public removeInterval(index: number) {
    this.intervals.splice(index, 1);

    this.segments = [];

    this.addInterval(this.intervals);
  }

  // removes then add an interval
  public editInterval(index: number, edited: WeeklyInterval) {
    this.removeInterval(index);
    this.addInterval(edited);
  }

  // removes the segment matching the input or by index
  public removeSegment(segment: number): boolean;
  public removeSegment(segment: number[]): boolean;
  public removeSegment(segment: any): boolean {

    var segmentIndex  = typeof segment === "object"
                      ? this.indexOfSegment(segment)
                      : segment;

    if (segmentIndex === -1)
      return false;
    this.segments.splice(segmentIndex, 1);
    return true;
  }

  // returns a table of days (mon to sun) containing an array of human readable strings
  public toWeekDays() : string[][] {
    var weekdays = new Array();
    var segments = this.truncate(3 * 60);

    for (var i = 0; i < segments.length; i ++) {
      var day = Math.floor(segments[i][0] / (24 * 60));
      if (!weekdays[day])
      weekdays[day] = new Array();
      weekdays[day].push(this.segmentToStr(segments[i]));
    }
    return weekdays;
  }

  // intended to input purposes (agregates by days having the same hours)
  public toWeeklyIntervals(): WeeklyInterval[] {
    var res: WeeklyInterval[] = [];
    var found = false;

    for (var a = 0; a < this.segments.length; a++) {
      var from = this.segments[a][0] % (24 * 60);
      var to = this.segments[a][1] % (24 * 60);
      var compareWith = new HourInterval(from, to);
      var day = Math.floor(this.segments[a][0] / (24 * 60));

      for (var z = 0; z < res.length; z++) {

        if (res[z].hours.isEqual(compareWith)) {
          res[z].addDay(day);
          found = true;
        }
      }
      if (!found) {
        var newInterval = new WeeklyInterval();
        newInterval.addDay(day);
        newInterval.setHours(new HourInterval(from, to));
        res.push(newInterval);
      }
      found = false;
    }
    return res;
  }

  public toString() {
    var res = "";
    for (var i = 0; i < this.segments.length; i++) {
      res += this.segments[i][0] + '-' + this.segments[i][1];
      if (i + 1 < this.segments.length)
        res += '|';
    }
    return res;
  }

  public isHappening(): boolean {
    var now = new RichDate().getMinutesFromMonday();

    for (var i = 0; i < this.segments.length; i++) {
      if (now >= this.segments[i][0] && now <= this.segments[i][1])
        return true;
    }
    return false;
  }

  // returns the concerned segment if is happenning
  public isHappeningOn(): boolean | number[] {
    var now = new RichDate().getMinutesFromMonday();

    for (var i = 0; i < this.segments.length; i++) {
      if (now >= this.segments[i][0] && now <= this.segments[i][1])
        return this.segments[i];
    }
    return false;
  }

  // returns the # minutes to the next change of state
  // wether it's the end of the current segment or the begining of the next one
  public changesIn(): number {
    if (this.segments.length === 0)
      return -1;

    var now = new RichDate().getMinutesFromMonday();

    var segmentFound = this.isHappeningOn();

    if (!segmentFound) {
      var nextSegment = this.segments[this.findNextSegment(now)];
      return (nextSegment[0] - now);
    }
    else
      return segmentFound[1] - now;
  }

  public changesAt(): RichDate | number {
    if (this.segments.length === 0)
      return -1;

    var now = new RichDate().getMinutesFromMonday();
    var segmentFound = this.isHappeningOn();

    if (!segmentFound) {
      var nextSegment = this.segments[this.findNextSegment(now)];
      return (new RichDate(nextSegment[0]));
    }
    else
      return new RichDate(segmentFound[1]);
  }

  /*
   #####  #####  # #    #   ##   ##### ######
   #    # #    # # #    #  #  #    #   #
   #    # #    # # #    # #    #   #   #####
   #####  #####  # #    # ######   #   #
   #      #   #  #  #  #  #    #   #   #
   #      #    # #   ##   #    #   #   ######
  */

  // delete the segments touching and returns the new extended one
  private mergeSegmentsIn(from: number, to: number): number[] {
    var res = [from, to];

    function flatTo(fromArg, toArg) {
      return (toArg < fromArg) ? (24 * 60 * 7) + toArg : toArg;
    }

    for (var i = 0; i < this.segments.length; i++) {

      var isExcluded = ((this.segments[i][0] < from && flatTo(this.segments[i][0], this.segments[i][1]) < from)
      || (from < this.segments[i][0] && flatTo(from, to) < this.segments[i][0]));

      if (!isExcluded) {
        if (this.segments[i][0] < res[0])
          res[0] = this.segments[i][0];
        if (flatTo(this.segments[i][0], this.segments[i][1]) > flatTo(res[0], res[1]))
          res[1] = this.segments[i][1];

        this.segments.splice(i, 1);
      }
    }
    return res;
  }

  // convert minutes to "hh:mm" format
  private minutesToStr(minutes: number) : string {
    var res = "";

    res = (Math.floor(minutes / 60)).toString();
    res += ":";
    res += (minutes % 60).toString();
    res += (minutes % 60 < 10) ? "0" : "";

    return res;
  }

  // convert segments to "hh:mm - hh:mm" format
  private segmentToStr(segment: number[]) : string {
    var hourFrom = segment[0] % (24 * 60);
    var hourTo = segment[1] % (24 * 60);

    return this.minutesToStr(hourFrom) + " - " + this.minutesToStr(hourTo);
  }

  // truncates to 24h intervals, used for displaying dayly intervals
  // we can specify a start for the day to truncate to (default midnight)
  private truncate(dayStartsAt: number = 0) {
    var res = [];
    var lengthDay = (24 * 60);

    for (var i = 0; i < this.segments.length; i++) {
      var day1 = Math.floor((this.segments[i][0] - dayStartsAt) / lengthDay);
      var day2 = Math.floor((this.segments[i][1] - dayStartsAt) / lengthDay);

      if (day1 !== day2) {
        var cutAt = (day1 === 6)
        ? dayStartsAt
        : day2 * lengthDay + dayStartsAt;

        res.push([this.segments[i][0], cutAt]);
        res.push([cutAt, this.segments[i][1]]);
      }
      else {
        res.push(this.segments[i]);
      }
    }
    return res;
  }

  // used to know where to insert a new segment
  private findPosition(from: number): number {
    for (var i = 0; i < this.segments.length; i++) {
      if (from < this.segments[i][0])
        return i;
    }
    return this.segments.length;
  }

  private findNextSegment(from: number): number {
    return  this.findPosition(from) === this.segments.length
            ? 0
            : this.findPosition(from);
  }
}
